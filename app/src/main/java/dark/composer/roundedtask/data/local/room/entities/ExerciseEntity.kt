package dark.composer.roundedtask.data.local.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "exercise")
data class ExerciseEntity(
    @PrimaryKey
    var id: Int,
    var name: String,
    var image: Int,
    var color:Int,
    var isCheck: Int = 1,
    var subTitle: String = "",
)