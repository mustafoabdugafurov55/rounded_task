package dark.composer.roundedtask.data.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dark.composer.roundedtask.data.local.room.AppDatabase
import dark.composer.roundedtask.data.local.room.InputDao
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

//    @Provides
//    @Singleton
//    fun provideDatabase(@ApplicationContext application: Application): AppDatabase{
//        return Room.databaseBuilder(application, AppDatabase::class.java, "exercise_db")
//            .fallbackToDestructiveMigration()
//            .build()
//    }

    @Provides
    fun provideArticleDao(db: AppDatabase): InputDao{
        return db.getInputData()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "RssReader"
        ).build()
    }
}
