package dark.composer.roundedtask.data.remote.models

data class TopicSubmissions(
    val architecture_interior: ArchitectureInterior
)