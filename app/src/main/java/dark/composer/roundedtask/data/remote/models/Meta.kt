package dark.composer.roundedtask.data.remote.models

data class Meta(
    val index: Boolean
)