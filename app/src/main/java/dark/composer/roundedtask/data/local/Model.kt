package dark.composer.roundedtask.data.local

data class Model (
    val name:String,
    val image:Int,
    val color:Int,
    val check:Int
)