package dark.composer.roundedtask.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dark.composer.roundedtask.data.base.BaseNetworkResult
import dark.composer.roundedtask.data.local.room.InputDao
import dark.composer.roundedtask.data.local.room.entities.ExerciseEntity
import dark.composer.roundedtask.data.remote.ApiService
import dark.composer.roundedtask.data.remote.models.ImageResponse
import dark.composer.roundedtask.domain.repo.MainRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val service: ApiService,
    private val dao: InputDao
) : MainRepository {
    override fun getRandomImages(): Flow<BaseNetworkResult<ImageResponse>> {
        return flow {
            val response = service.getRandomImages()
            emit(BaseNetworkResult.Loading(true))
            if (response.isSuccessful) {
                emit(BaseNetworkResult.Loading(false))
                response.body()?.let {
                    emit(BaseNetworkResult.Success(it))
                }
            } else {
                emit(BaseNetworkResult.Loading(false))
                emit(BaseNetworkResult.Error(response.message()))
            }
        }
    }

    override fun getExercise(): Flow<ArrayList<ExerciseEntity>> = flow {
        dao.getAllTrainers().let {
            val r = ArrayList<ExerciseEntity>()
            it.forEach { o ->
                r.add(o)
            }
            Log.d("WWWWW", "getExercise: $r")
            emit(r)
        }
    }

    override fun addExercises(list: ArrayList<ExerciseEntity>) {
        dao.saveTrainers(list)
    }

    override fun updateExercises(id: Int, isCheck: Int):Flow<String> = flow {
        if (dao.updateUser(id, isCheck) > 0){
            emit("Success")
            Log.d("NNNNNN", "updateExercises: Success")
        }else{
            Log.d("NNNNNN", "updateExercises: Error")
            emit("Error")
        }
        Log.d("NNNNNN", "updateExercises: Keldi")
    }
}

